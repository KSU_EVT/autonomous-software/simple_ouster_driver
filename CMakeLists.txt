cmake_minimum_required(VERSION 3.8)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/ouster_sdk/cmake) # ouster_sdk is a submodule

project(simple_ouster_driver)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
set(CMAKE_C_FLAGS_DEBUG "-O0 -g")


# find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_ros REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(OusterSDK REQUIRED)

add_library(simple_ouster_driver
  src/simple_ouster_driver_node.cpp)
target_compile_features(simple_ouster_driver PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
target_include_directories(simple_ouster_driver PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(simple_ouster_driver
  PUBLIC
    OusterSDK::ouster_client)
ament_target_dependencies(simple_ouster_driver
  PUBLIC
    rclcpp
    rclcpp_components
    sensor_msgs)

# Causes the visibility macros to use dllexport rather than dllimport,
# which is appropriate when building the dll but not consuming it.
#target_compile_definitions(simple_ouster_driver PRIVATE "SIMPLE_OUSTER_DRIVER_BUILDING_LIBRARY")

install(
  DIRECTORY include/
  DESTINATION include
)
install(
  TARGETS simple_ouster_driver
  EXPORT export_${PROJECT_NAME}
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)

install(
  IMPORTED_RUNTIME_ARTIFACTS ouster_client # we have to use the non-alias name
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)
  

add_executable(simple_ouster_driver_node src/main.cpp)
target_compile_features(simple_ouster_driver PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
target_include_directories(simple_ouster_driver_node PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)
target_link_libraries(simple_ouster_driver_node simple_ouster_driver)

install(TARGETS simple_ouster_driver_node
  DESTINATION lib/${PROJECT_NAME})

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)
#install(DIRECTORY
#  config
#  DESTINATION share/${PROJECT_NAME}/
#)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # comment the line when a copyright and license is added to all source files
  set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # comment the line when this package is in a git repo and when
  # a copyright and license is added to all source files
  set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

ament_export_include_directories(
  include
)
ament_export_libraries(
  simple_ouster_driver
)
ament_export_targets(
  export_${PROJECT_NAME}
)

ament_package()
