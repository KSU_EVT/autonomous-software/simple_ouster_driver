#pragma once

#include <memory>
#include <mutex>
#include <queue>
#include <rclcpp/node.hpp>
#include <rclcpp/node_options.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>

#include "synchronized_unique_ptr_queue.hpp"

// The ouster headers contain sooo many deprecated declarations; we don't even use them directly
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wpedantic"
#include <ouster/client.h>
#include "ouster/lidar_scan.h"
#pragma GCC diagnostic pop

namespace simple_ouster_driver {

struct ConvertedScan {
  int32_t scan_id; // used for reordering
  sensor_msgs::msg::PointCloud2 cloud;
};

class SimpleOusterDriverNode : public rclcpp::Node {
private:
  std::shared_ptr<ouster::sensor::client> client_;
  std::unique_ptr<ouster::ScanBatcher> scan_batcher_;
  ouster::sensor::sensor_info info_;
  std::unique_ptr<const ouster::sensor::packet_format> packet_format_; // help me
  ouster::XYZLut xyz_lut_;

  SynchronizedUniquePtrQueue<ouster::LidarScan> processing_queue_;
  SynchronizedUniquePtrQueue<ConvertedScan> publishing_queue_;

  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr points_pub_;

  bool use_ros_time_;
public:
  SimpleOusterDriverNode();
  SimpleOusterDriverNode(const rclcpp::NodeOptions& options);

  ~SimpleOusterDriverNode();

  // the following methods are synchronized; they can be called in seperate threads
  void receiveData();
  void processData();
};

}  // namespace simple_ouster_driver
