#pragma once

#include <memory>
#include "synchronized_unique_ptr_queue.hpp"

namespace simple_ouster_driver {

template<typename T>
std::unique_ptr<T> SynchronizedUniquePtrQueue<T>::try_send(std::unique_ptr<T> work) {
  if (!queue_mutex_.try_lock()) {
    return work; // give the unique_ptr back to the calling scope if we couldn't send; they should try again later
  }

  work_queue_.push(std::move(work));
  queue_mutex_.unlock();
  queue_cv_.notify_one();

  return nullptr;
}

template<typename T>
void SynchronizedUniquePtrQueue<T>::send(std::unique_ptr<T> work) {
  queue_mutex_.lock();
  work_queue_.push(std::move(work));
  queue_mutex_.unlock();

  queue_cv_.notify_one();
}

template<typename T>
std::unique_ptr<T> SynchronizedUniquePtrQueue<T>::take() {
  std::unique_ptr<T> scan_ptr;

  {
    std::unique_lock<std::mutex> lock(queue_mutex_);
    queue_cv_.wait(lock, [this]{return !work_queue_.empty();});

    scan_ptr = std::move(work_queue_.front());
    work_queue_.pop();
  } 

  return scan_ptr;
}

}
