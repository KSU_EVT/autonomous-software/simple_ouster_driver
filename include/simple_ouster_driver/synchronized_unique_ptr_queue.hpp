#pragma once

#include <memory>
#include <queue> 
#include <mutex>
#include <condition_variable>

namespace simple_ouster_driver {

template<typename T>
class SynchronizedUniquePtrQueue {
private:
  std::queue<std::unique_ptr<T>> work_queue_;
  std::mutex queue_mutex_;
  std::condition_variable queue_cv_;

public:
  // non-blocking
  // if lock acquire fails, trySendScan will give the unique_ptr back
  std::unique_ptr<T> try_send(std::unique_ptr<T> work);

  // blocking
  void send(std::unique_ptr<T> work);

  // blocking
  std::unique_ptr<T> take();
};

}

#include "synchronized_unique_ptr_queue.tpp"
