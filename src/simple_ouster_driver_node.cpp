#include "simple_ouster_driver/simple_ouster_driver_node.hpp"

#include <atomic>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <rclcpp/logging.hpp>
#include <rclcpp/qos.hpp>

#include "ouster/client.h"

// The ouster headers contain sooo many deprecated declarations; we don't even
// use them directly
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wpedantic"
#include "ouster/lidar_scan.h"
#include "ouster/types.h"
#pragma GCC diagnostic pop

namespace simple_ouster_driver {

constexpr size_t UDP_BUF_SIZE = 65536;

SimpleOusterDriverNode::SimpleOusterDriverNode()
    : SimpleOusterDriverNode(rclcpp::NodeOptions()){};

SimpleOusterDriverNode::SimpleOusterDriverNode(
    const rclcpp::NodeOptions& options)
    : rclcpp::Node("simple_ouster_driver", options) {
  points_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>(
      "points", rclcpp::SensorDataQoS());

  client_ = ouster::sensor::init_client("10.0.0.9", 7502, 7503);
  if (!client_) {
    RCLCPP_FATAL(this->get_logger(), "Failed to connect to sensor");
  }
  RCLCPP_INFO(this->get_logger(), "Connected to sensor");

  RCLCPP_INFO(this->get_logger(), "Gathering metadata...");
  info_ =
      ouster::sensor::parse_metadata(ouster::sensor::get_metadata(*client_));
  packet_format_ =
      std::make_unique<ouster::sensor::packet_format>(info_);  // shoot me

  size_t w = info_.format.columns_per_frame;
  size_t h = info_.format.pixels_per_column;

  ouster::sensor::ColumnWindow column_window = info_.format.column_window;
  RCLCPP_INFO_STREAM(this->get_logger(),
                     "  Firmware version:  "
                         << info_.fw_rev
                         << "\n  Serial number:     " << info_.sn
                         << "\n  Product line:      " << info_.prod_line
                         << "\n  Scan dimensions:   " << w << " x " << h
                         << "\n  Column window:     [" << column_window.first
                         << ", " << column_window.second << "]" << std::endl);

  // A ScanBatcher can be used to batch packets into scans
  scan_batcher_ = std::make_unique<ouster::ScanBatcher>(info_);

  xyz_lut_ = ouster::make_xyz_lut(info_);

  // whether to use ROS time or Ouster time
  use_ros_time_ = (bool)this->declare_parameter<bool>("use_ros_time", false);
}

SimpleOusterDriverNode::~SimpleOusterDriverNode() = default;

void SimpleOusterDriverNode::receiveData() {
  // buffer to store raw udp packet data
  auto packet_buf = std::make_unique<uint8_t[]>(UDP_BUF_SIZE);

  // heap allocated scan; will eventually be put in our processing queue
  auto scan = std::make_unique<ouster::LidarScan>(
      info_.format.columns_per_frame, info_.format.pixels_per_column,
      info_.format.udp_profile_lidar);
  std::unique_ptr<ouster::LidarScan> try_again_scan = nullptr;

  while (true) {
    // polls sensor status, don't block
    ouster::sensor::client_state st = ouster::sensor::poll_client(*client_, 0);

    if (try_again_scan) {
      try_again_scan = processing_queue_.try_send(std::move(try_again_scan));
      // if trySendScan succeeds, last_scan == nullptr
    }

    // check for error status
    if (st & ouster::sensor::CLIENT_ERROR) {
      RCLCPP_FATAL(this->get_logger(), "Sensor client returned error state!");
    }

    // check for lidar data, read a packet and add it to the current scan batch
    if (st & ouster::sensor::LIDAR_DATA) {
      if (!ouster::sensor::read_lidar_packet(*client_, packet_buf.get(),
                                             *packet_format_)) {
        RCLCPP_FATAL(this->get_logger(),
                     "Failed to read a packet of the expected size!");
      }

      // batcher will return "true" when the scan is complete
      if (scan_batcher_->operator()(packet_buf.get(), *scan)) {  // shoot me
        // retry until we receive a full set of valid measurements
        if (scan->complete(info_.format.column_window)) {
          try_again_scan = processing_queue_.try_send(std::move(scan));
          // now scan == nullptr; if trySendScan fails, last_scan == the old
          // value of scan, else nullptr

          scan = std::make_unique<ouster::LidarScan>(
              info_.format.columns_per_frame, info_.format.pixels_per_column,
              info_.format.udp_profile_lidar);
          // now scan is pointing to a newly allocated LidarScan, which we can
          // batch new packets into
        }
      }
    }
  }
}

#include <chrono>
#include <ratio>

void SimpleOusterDriverNode::processData() {
  sensor_msgs::msg::PointCloud2 msg;

  msg.header.frame_id = "lidar";  // FIXME

  int offset = 0;
  int bytes_per_point = 0;
  msg.fields.emplace_back();
  msg.fields.back().name = "x";
  msg.fields.back().offset = (offset++) * sizeof(float);
  msg.fields.back().datatype = sensor_msgs::msg::PointField::FLOAT32;
  bytes_per_point += sizeof(float);
  msg.fields.back().count = 1;
  msg.fields.emplace_back();
  msg.fields.back().name = "y";
  msg.fields.back().offset = (offset++) * sizeof(float);
  msg.fields.back().datatype = sensor_msgs::msg::PointField::FLOAT32;
  bytes_per_point += sizeof(float);
  msg.fields.back().count = 1;
  msg.fields.emplace_back();
  msg.fields.back().name = "z";
  msg.fields.back().offset = (offset++) * sizeof(float);
  msg.fields.back().datatype = sensor_msgs::msg::PointField::FLOAT32;
  bytes_per_point += sizeof(float);
  msg.fields.back().count = 1;
  msg.fields.emplace_back();
  msg.fields.back().name = "intensity";
  msg.fields.back().offset = (offset++) * sizeof(float);
  msg.fields.back().datatype = sensor_msgs::msg::PointField::UINT32;
  bytes_per_point += sizeof(uint32_t);
  msg.fields.back().count = 1;

  const int num_points =
      info_.format.columns_per_frame * info_.format.pixels_per_column;
  const size_t points_bytes = num_points * bytes_per_point;
  msg.data.resize(points_bytes);
  std::printf("data size: %zu\n", msg.data.size());
  msg.width = num_points;
  msg.height = 1;
  msg.row_step = points_bytes;

  msg.point_step = offset * sizeof(float);
  msg.is_bigendian = false;
  msg.is_dense = false;

  while (true) {
    // blocks until there's a scan in the queue
    std::unique_ptr<ouster::LidarScan> scan = processing_queue_.take();

    if (use_ros_time_) {
      msg.header.stamp = this->get_clock()->now();
    } else {
      auto ts_nanosecs = *(std::find_if(
          scan->timestamp().begin(), scan->timestamp().end(),
          [](const auto& ts_nanosecs) { return ts_nanosecs != 0; }));
      msg.header.stamp.sec = ts_nanosecs / 1'000'000'000;
      msg.header.stamp.nanosec = ts_nanosecs % 1'000'000'000;
    }

    auto cart_t0 = std::chrono::high_resolution_clock::now();
    auto cart_points = ouster::cartesian(*scan, xyz_lut_);
    auto cart_t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> cart_ms = cart_t1 - cart_t0;
    auto signals = scan->field(ouster::sensor::ChanField::SIGNAL);
    // std::printf("signals rows: %zu\n", signals.rows());
    // std::printf("signals cols: %zu\n", signals.cols());

    auto row_t0 = std::chrono::high_resolution_clock::now();
    // points is in column major order, we have to convert to row major in our
    // message

    uint32_t* raw_signals = signals.data();
    uint8_t* raw_msg_data = msg.data.data();
    for (std::ptrdiff_t i = 0; i < cart_points.rows(); i++) {
      // add the xyz fields
      for (std::ptrdiff_t j = 0; j < 3; j++) {
        // el is an x, y, or z value for a point
        float el = cart_points(i, j);

        // uint8_t* data = &msg.data[index++ * sizeof(float)];
        // std::memcpy(data, &el, sizeof(float));
        //  add our float to our data vector as four bytes
        std::memcpy(raw_msg_data, &el, sizeof(float));
        raw_msg_data += sizeof(float);
      }

      // add intensity field
      std::memcpy(raw_msg_data, raw_signals++, sizeof(uint32_t));
      raw_msg_data += sizeof(uint32_t);
    }
    auto row_t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> row_ms = row_t1 - row_t0;

    points_pub_->publish(msg);

    std::printf("cart: %fms; row: %fms; Published scan! Frame ID: %d\n",
                cart_ms.count(), row_ms.count(), scan->frame_id);
  }
}

}  // namespace simple_ouster_driver
